![img](themes/reinedeselfes/img/favicon.png)


## Ceci est un fork de 

# Last 

de  [Luc Didry](https://fiat-tux.fr) 

**C'est pour moi l'occasion d'essayer git et de surcroît regrouper mes mercredifictions.**

Je garde la base du readme de Luc, parceque j'ai peur d'oublier des trucs.

En même temps comme c'est écrit en Anglais, je ne comprends pas tout, voire, pas grand chose 

Moi, je suis lareinedeselfes, une non informaticienne.... Je pige rien mais j'aime bien essayer des trucs.

# Mes mercredifictions sont ici 

<https://lareinedeselfes.frama.io/last/>


Et le readme de Luc est là :) 


![Last logo: a viking hat](themes/default/img/favicon.png)

This is a script designed to run on [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/index.html).

It fetches toots from [Mastodon](https://github.com/tootsuite/mastodon) and builds a webpage, an atom feed and an epub with them.

Last stands for Let's Aggregate Superb Toots. (Thanks to [@Pouhiou](https://framapiaf.org/@Pouhiou) for the name)

The [logo](https://openclipart.org/detail/267534/viking-hat) comes from [Carolemagnet](https://openclipart.org/user-detail/carolemagnet), which released it in the public domain. Thanks!

## How to use?

Fork this project, modify `last.conf`, then commit and push. Enjoy.

To add new toots, add them in `urls` and then commit and push. You can merge multiple toots by adding them like this:

```
        [ 'https://framapiaf.org/@framasky/99445908349445234', 'https://framapiaf.org/@framasky/99445923365976338' ],
```

If you want to change the aspect of the generated web page and epub, copy the default theme to a new directory in `themes/`, modify it and choose this new theme in the configuration file.

If you want to handle [Markdown syntax](https://daringfireball.net/projects/markdown/syntax) in your toots, you'll need to install pandoc.

If you want to modify the content of the toots, modify `LastCustom.pm` file.

## Example

Please, go to <https://luc.frama.io/last/> to see how it looks.

## License

MIT Licence, see the [LICENSE](LICENSE) file for details. 


## Author

[Luc Didry](https://fiat-tux.fr). You can support me on [Tipeee](https://tipeee.com/fiat-tux) and [Liberapay](https://liberapay.com/sky).

![Tipeee button](themes/default/img/tipeee-tip-btn.png) ![Liberapay logo](themes/default/img/liberapay.png)
