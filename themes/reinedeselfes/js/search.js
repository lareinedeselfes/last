function searchInitialize() {
    let elr = elasticlunr(function() {
        this.addField('text');
        this.addField('alt');
        this.addField('poll');
        this.setRef('id');
    });
    return {
        searchTerms: null,
        toots: [],
        tootsMatching: [],
        tootsFetched: false,
        elr: elr,
        getTootJson() {
            let myThis = this;
            fetch('js/search.json')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then((res) => {
                myThis.toots        = res;
                myThis.tootsFetched = true;
                for (let i in res) {
                    let item = res[i];
                    elr.addDoc({
                        "id":   item.id,
                        "text": item.text,
                        "alt":  item.alt,
                        "poll": item.poll
                    });
                }
            });
        },
        changeSearch() {
            if (this.searchTerms === '' || this.searchTerms === null) {
                this.tootsMatching = [];
            } else {
                let results = this.elr.search(this.searchTerms)
                this.tootsMatching = [];
                let myThis  = this;
                results.forEach((item) => {
                    myThis.tootsMatching.push(item.ref);
                });
            }
        }
    }
}
